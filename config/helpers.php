<?php
function dd($data)
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}

function redirect($path)
{
    return header('location:'.$path);
}